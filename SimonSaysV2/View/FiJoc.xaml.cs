﻿using SimonSaysV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SymonSaisV2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FiJoc : ContentPage
    {
        public FiJoc()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        public FiJoc(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
    }
}