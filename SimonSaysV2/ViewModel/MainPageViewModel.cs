﻿using SymonSaisV2;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace SimonSaysV2
{
    class MainPageViewModel : NavigationPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand J1_clickGroc { get; set; }
        public ICommand J1_clickBlau { get; set; }
        public ICommand J1_clickVerd { get; set; }
        public ICommand J1_clickVermell { get; set; }
        public ICommand J1_iniciJoc { get; set; }
        public ICommand J1_finalJoc { get; set; }



        //Inicialitzar Random 
        Random j1_rand = new Random();

        //Agreguem un numero a cada color per poder guarda el moguiments d'intre d'un array
        public String j1_groc = "1";
        public String j1_verd = "2";
        public String j1_vermell = "3";
        public String j1_blau = "4";

        ArrayList j1_SimonSais = new ArrayList(); //Crem l'array on guardarem el patro del joc
        ArrayList j1_SimonSaisJugador = new ArrayList();//Crem l'array on guardarem els muguiments del jugador

        public String J1_visibilidad
        {
            set
            {
                if (j1_visibilidad != value) { j1_visibilidad = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J1_visibilidad")); } }
            }
            get { return j1_visibilidad; }
        }
        public String j1_visibilidad = "True"; //Habilitar o desabilitar la imatge "IA TURN"

        public String J1_visibilidadJugador
        {
            set
            {
                if (j1_visibilidadJugador != value) { j1_visibilidadJugador = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J1_visibilidadJugador")); } }
            }
            get { return j1_visibilidadJugador; }
        }
        public String j1_visibilidadJugador = "False"; //Habilitar o desabilitar imatge "Turno Jugador"

        public String J1_click
        {
            set
            {
                if (j1_click != value) { j1_click = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J1_click")); } }
            }
            get { return j1_click; }
        }
        public String j1_click = "False"; //HABILITAR O DESABILITAR EL CLICK

        ContentPage j1_nav;

        //Modificar la opacitat de les imatges
        public float J1_opacitatVermell
        {
            set
            {
                if (j1_opacitatVermell != value) { j1_opacitatVermell = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J1_opacitatVermell")); } }
            }
            get { return j1_opacitatVermell; }
        }
        public float j1_opacitatVermell = 1;

        public float J1_opacitatVerd
        {
            set
            {
                if (j1_opacitatVerd != value) { j1_opacitatVerd = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J1_opacitatVerd")); } }
            }
            get { return j1_opacitatVerd; }
        }
        public float j1_opacitatVerd = 1;

        public float J1_opacitatGroc
        {
            set
            {
                if (j1_opacitatGroc != value) { j1_opacitatGroc = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J1_opacitatGroc")); } }
            }
            get { return j1_opacitatGroc; }
        }
        public float j1_opacitatGroc = 1;

        public float J1_opacitatBlau
        {
            set
            {
                if (j1_opacitatBlau != value) { j1_opacitatBlau = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J1_opacitatBlau")); } }
            }
            get { return j1_opacitatBlau; }
        }
        public float j1_opacitatBlau = 1;

        public Boolean j1_flag = true;
        public int j1_punts = 0;
        public int J1_punts
        {
            set
            {
                if (j1_punts != value) { j1_punts = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J1_punts")); } }
            }
            get { return j1_punts; }
        }
        String j1_numString;

        public MainPageViewModel(ContentPage navi) //Al iniciar el programa entrem aquesta funcio i executem les funcions que ens interesa
        {
            j1_nav = navi;
            //Funcions que s'activen quan fas click a les imatges

            J1_clickGroc = new Command(ComandoGroc);
            J1_clickBlau = new Command(ComandoBlau);
            J1_clickVerd = new Command(ComandoVerd);
            J1_clickVermell = new Command(ComandoVermell);
            J1_iniciJoc = new Command(ComandoInici);
            J1_finalJoc = new Command(ComandoFijoc);
            JuegoAsync();
        }


        public async Task JuegoAsync()
        {
                J1_click = "False"; //Desabilitem els click per que no es el torn del jugador
                await Task.Delay(2000);
                J1_visibilidad = "False"; // Desabilitem la imatge de que es el turn de la ia
                await Task.Delay(500);
                int numero = j1_rand.Next(1, 4); //Cream el numero random que sera el que farem servir pel patro.
                j1_numString = numero.ToString();
               
                j1_SimonSais.Add(j1_numString); //Afegir color array list
                
                foreach (String j1_x in j1_SimonSais)
                { //Recorrem els colors de la sequencia del jo
                    CanviColorsAsync(j1_x);
                    await Task.Delay(1500);
                }
                //turno jugador
                await Task.Delay(500);
                J1_visibilidadJugador = "True"; //Habilitem la imatge de que toca jugar al jugador
                await Task.Delay(500);
                J1_visibilidadJugador = "False"; //Desabilitem la imatge de que toca al jugador
                J1_click = "True";
           
         


        }


        //Canvi Colors IA
        public async Task CanviColorsAsync(String x) //Canviar la opacitat de la imatge per part de la IA
        {
            if (x == j1_groc) //Passem el numero random per i l·luminar l'imatge
            {
                J1_opacitatGroc = 0.5f;
                await Task.Delay(700);
                J1_opacitatGroc = 1;
            }
            if (x == j1_verd)
            {
                J1_opacitatVerd = 0.5f;
                await Task.Delay(700);
                J1_opacitatVerd = 1;
            }
            if (x == j1_vermell)
            {
                J1_opacitatVermell = 0.5f;
                await Task.Delay(700);
                J1_opacitatVermell = 1;
            }
            if (x == j1_blau)
            {
                J1_opacitatBlau = 0.5f;
                await Task.Delay(700);
                J1_opacitatBlau = 1;
            }
        }

        public async Task ComprovacionAsync() //Comprovem que els moguiments del jugador siguin correctes, 
        {
            for (int j1_i = 0; j1_i< j1_SimonSaisJugador.Count; j1_i++)
            {
                if (j1_SimonSaisJugador[j1_i].Equals(j1_SimonSais[j1_i]))
                {
                }
                else //Si el jugador s'equivoca el joc s'acaba
                { 
                    Final();
                }
                if (j1_SimonSais.Count == j1_SimonSaisJugador.Count) //Si la array del jugador es igual que l'array del joc passa de ronda
                {

                    await Task.Delay(500);
                    J1_visibilidad = "True"; // Desabilitem la imatge de que es el turn de la ia
                    JuegoAsync();
                    j1_SimonSaisJugador = new ArrayList(); // Resetejar Array List per poder seguir jugant
                    J1_punts++;
                    J1_click = "False";
                }

            }
        }



        //Vermell
        private void ComandoVermell(object obj)
        {
            j1_SimonSaisJugador.Add(j1_vermell);
            CanvioOpacitatVermell();
            ComprovacionAsync();
        }

        public async Task CanvioOpacitatVermell()  //Camviem les imatges i agreguem el click que a fet el usuari
        {
            J1_opacitatVermell = 0.5f;
            await Task.Delay(500);
            J1_opacitatVermell = 1;
        }


        //Verd
        private void ComandoVerd(object obj)
        {
            j1_SimonSaisJugador.Add(j1_verd);
            CanvioOpacitatVerd();
            ComprovacionAsync();
        }
        public async Task CanvioOpacitatVerd()  //Camviem les imatges i agreguem el click que a fet el usuari
        {
            J1_opacitatVerd = 0.5f;
            await Task.Delay(500);
            J1_opacitatVerd = 1;
        }


        //Blau
        private void ComandoBlau(object obj)
        {
            j1_SimonSaisJugador.Add(j1_blau);
            CanvioOpacitatBlau();
            ComprovacionAsync();
        }
        public async Task CanvioOpacitatBlau()  //Camviem les imatges i agreguem el click que a fet el usuari
        {
            J1_opacitatBlau = 0.5f;
            await Task.Delay(500);
            J1_opacitatBlau = 1;
        }


        //Groc
        private void ComandoGroc(object obj)
        {
            j1_SimonSaisJugador.Add(j1_groc);
            CanvioOpacitatGroc();
            ComprovacionAsync();
        }

        public async Task CanvioOpacitatGroc()  //Camviem les imatges i agreguem el click que a fet el usuari
        {
            J1_opacitatGroc = 0.5f;
            await Task.Delay(500);
            J1_opacitatGroc = 1;
        }
        private async void ComandoFijoc()
        {
            await j1_nav.Navigation.PushModalAsync(new Inici()); //Un cop acabat el pitgem per tornar al inici per reiniciar el joc
        }
        private async void ComandoInici()
        {
            await j1_nav.Navigation.PushModalAsync(new MainPage()); //Accedir a la pagina per a puguer jugar
        }
        private async void Final()
        {
            await j1_nav.Navigation.PushModalAsync(new FiJoc()); // Accedir a la pagina del Final del Joc
        }

    }

    

}
